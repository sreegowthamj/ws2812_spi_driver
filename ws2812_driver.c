/**
 *   \file ws2812_driver.c
 *   \brief SPI driver for controlling WS2812 device
 *
 *  SPI driver for controlling WS2812 device. Generates a circular
 *  pattern on the 16 led device. Configurable from user space through write
 *  and IOCTL operations
 */

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/spi/spi.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/ioctl.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/err.h>
#include <linux/list.h>
#include <linux/errno.h>
#include <linux/of.h>
#include <linux/of_device.h>
#include <asm/uaccess.h>
#include <linux/string.h>
#include <linux/delay.h>
#include <linux/gpio.h>

/* Has IOCTL definition for RESET ********************************************/
#include "ws2812_consts.h"

#define WS2812_MAJOR 154 /* assigned for SPI */
#define N_SPI_MINORS 32

const uint8_t SET_HIGH = 120; //140
const uint8_t SET_LOW = 20; //20

static DECLARE_BITMAP(minors, N_SPI_MINORS);
static unsigned bufsiz = 16 * 3 * 8;
module_param(bufsiz, uint, S_IRUGO);
MODULE_PARM_DESC(bufsiz, "data bytes in biggest supported SPI message");
/* For testing purposes only, spi_board_info struct data ***************/
static int busnum = 1;
module_param(busnum, int, S_IRUGO);
MODULE_PARM_DESC(busnum, "bus num of the controller");
static int chipselect = 1;
module_param(chipselect, int, S_IRUGO);
MODULE_PARM_DESC(chipselect, "chip select of the desired device");
static int maxspeed = 7000000; //2000000
module_param(maxspeed, int, S_IRUGO);
MODULE_PARM_DESC(maxspeed, "max_speed of the desired device");
static int spimode = SPI_MODE_0;
module_param(spimode, int, S_IRUGO);
MODULE_PARM_DESC(spimode, "mode of the desired device");
static struct spi_device *spi;
/* Creates class for creating character device nodes to expose to user space */
static struct class *ws2812_class;
/* Device Specific Data ******************************************************/
struct ws2812_data {
	dev_t devt;
	spinlock_t spi_lock;
	struct spi_device *spi;
	struct list_head device_entry;

	/* TX/RX buffers are NULL unless this device is open (users > 0) */
	struct mutex buf_lock;
	unsigned users;
	u8 *tx_buffer;
	//	u8 *rx_buffer;
	u32 speed_hz;
};


typedef struct led_tx_data {
	char data_bit_expanded[3][8];
} led_tx_data;

/* Maintains the list of devices accessing this driver ***********************/
static LIST_HEAD(device_list);
static DEFINE_MUTEX(device_list_lock);

static ssize_t ws2812_sync(struct ws2812_data *ws2812,
			   struct spi_message *message)
{
	int status;
	struct spi_device *spi;

	spin_lock_irq(&ws2812->spi_lock);
	spi = ws2812->spi;

	if (spi == NULL)
		status = -ESHUTDOWN;
	else
		status = spi_sync(spi, message);
	spin_unlock_irq(&ws2812->spi_lock);

	if (status == 0)
		status = message->actual_length;

	return status;
}

static inline ssize_t ws2812_sync_write(struct ws2812_data *ws2812, size_t len)
{
	struct spi_transfer t = {
		.tx_buf = ws2812->tx_buffer,
		.len = len,
		.speed_hz = ws2812->speed_hz,
	};
	struct spi_message m;

	spi_message_init(&m);
	spi_message_add_tail(&t, &m);
	return ws2812_sync(ws2812, &m);
}

void byte_encode_tx_data(led_prop *src, led_tx_data *dst)
{
	int i;
	uint8_t j, k, count = 8;
	for (i = 2; i >= 0; i--) {
		j = src->color[i];
//		pr_info("Source number = %d", j);
		count = 8;
		while (count != 0) {
			//			k = ((j << 1) >> 1);
			//			k = k - j;
			//			k = k >> 7;
			k = j - ((j >> 1) << 1);
			if (k == 1) {
//				pr_info("Color = %d, value = HIGH, bit = %d", i,
//					count);
				dst->data_bit_expanded[i][--count] = SET_HIGH;
			} else if (k == 0) {
//				pr_info("Color = %d, value = LOW, bit = %d", i,
//					count);
				dst->data_bit_expanded[i][--count] = SET_LOW;
			} else {
				--count;
				pr_err("Error in bit calculation");
			}
			j = j >> 1;
		}
	}
}

static ssize_t ws2812_write(struct file *filp, const char __user *buf,
			    size_t count, loff_t *f_pos)
{
	led_tx_data *tx_data;
	struct ws2812_data *ws2812;
	ssize_t status = 0;
	int i = 0/* , j = 0 */;
	unsigned long missing;
	led_strip z;
	int data_size = 0;
	//	pr_info("Write invoked");

	/* chipselect only toggles at start or end of operation */
	if (count > bufsiz)
		return -EMSGSIZE;

	ws2812 = filp->private_data;
	missing = copy_from_user(&z, buf, count);
	data_size = sizeof(led_prop) * (z.n);
	//	pr_info("Size of data received = %d, data being written = %d ",
	// count, 		data_size * 8);
	tx_data = kmalloc(data_size * 8,
			  GFP_KERNEL); // 8 is for the number of bits in a byte
	for (i = 0; i < z.n; i++) {
		byte_encode_tx_data(&z.led[i], &tx_data[i]);
	}

	mutex_lock(&ws2812->buf_lock);
	memcpy(ws2812->tx_buffer, tx_data, 8 * data_size);
	if (missing == 0) {
		status = ws2812_sync_write(ws2812, data_size * 8);
		usleep_range(55, 60);
	} else
		status = -EFAULT;
	mutex_unlock(&ws2812->buf_lock);

	kfree(tx_data);

	return status;
}

static ssize_t ws2812_read(struct file *filp, char __user *buf, size_t count,
			   loff_t *f_pos)
{
	ssize_t status = 0;
	pr_info("Read invoked \n");
	return status;
}

static long ws2812_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	long retval = 0;
	pr_info("IOCTL op invoked \n");
	switch (cmd) {
	case WS2812_RESET:
		pr_info("Reset IOCTL called");
                usleep_range(55, 60);
		break;
	default:
		pr_err("Invalid IOCTL call");
	}

	return retval;
}

static int ws2812_open(struct inode *inode, struct file *filp)
{
	int status = -ENXIO;
	struct ws2812_data *ws2812;
	pr_info("Open invoked \n");

	mutex_lock(&device_list_lock);

	list_for_each_entry(ws2812, &device_list, device_entry)
	{
		if (ws2812->devt == inode->i_rdev) {
			status = 0;
			break;
		}
	}
	if (status) {
		pr_err("Device not found in list\n");
		goto err_find_dev;
	}

	if (!ws2812->tx_buffer) {
		ws2812->tx_buffer = kmalloc(bufsiz, GFP_KERNEL);
		if (!ws2812->tx_buffer) {
			pr_err("unable to get memory for tx buffer\n");
			status = -ENOMEM;
			goto err_find_dev;
		}
	}
	ws2812->users++;
	filp->private_data = ws2812;
	nonseekable_open(inode, filp);
	mutex_unlock(&device_list_lock);
	return 0;

err_find_dev:
	mutex_unlock(&device_list_lock);
	return status;
}

static int ws2812_release(struct inode *inode, struct file *filp)
{
	struct ws2812_data *ws2812;

	pr_info("Release invoked");
	mutex_lock(&device_list_lock);
	ws2812 = filp->private_data;
	filp->private_data = NULL;

	/* last close? */
	ws2812->users--;
	if (!ws2812->users) {
		int dofree;

		kfree(ws2812->tx_buffer);
		ws2812->tx_buffer = NULL;

		spin_lock_irq(&ws2812->spi_lock);
		if (ws2812->spi)
			ws2812->speed_hz = ws2812->spi->max_speed_hz;

		/* after we unbound from the underlying device? */
		dofree = (ws2812->spi == NULL);
		spin_unlock_irq(&ws2812->spi_lock);

		if (dofree)
			kfree(ws2812);
	}
	mutex_unlock(&device_list_lock);

	return 0;
}

static const struct file_operations ws2812_fops = {
	.owner = THIS_MODULE,
	.write = ws2812_write,
	.read = ws2812_read,
	.unlocked_ioctl = ws2812_ioctl,
	.open = ws2812_open,
	.release = ws2812_release,
};

static void free_exportd_gpios(void)
{
	gpio_free(24);
	gpio_free(44);
	gpio_free(72);
	gpio_free(25);
}

static int config_gpio(void)
{
	int status;
	/* Configuring GPIO PINS to make IO11 the SPI MOSI *******************/
	status = gpio_request(24, "sysfs");
	if (status != 0)
		pr_warn("GPIO allocation failed for GPIO pin = 24 \n");
	status = gpio_direction_output(24, 0);
	if (status != 0)
		pr_warn("gpio_set_direction failed for GPIO pin = 24 \n");
	gpio_set_value_cansleep(24, 0);

	status = gpio_request(25, "sysfs");
	if (status != 0)
		pr_warn("GPIO allocation failed for GPIO pin = 25 \n");
        status = gpio_direction_input(25);
	if (status != 0)
		pr_warn("gpio_set_direction failed for GPIO pin = 25 \n");

	status = gpio_request(44, "sysfs");
	if (status != 0)
		pr_warn("GPIO allocation failed for GPIO pin = 44 \n");
	gpio_set_value_cansleep(44, 1);

	status = gpio_request(72, "sysfs");
	if (status != 0)
		pr_warn("GPIO allocation failed for GPIO pin = 72 \n");
	gpio_set_value_cansleep(72, 0);

	return status;
}

static int ws2812_probe(struct spi_device *spi)
{
	int status = 0;
	struct ws2812_data *ws2812;
	unsigned long minor;
	pr_info("Probe invoked");
	ws2812 = kzalloc(sizeof(struct ws2812_data), GFP_KERNEL);
	if (!ws2812)
		return -ENOMEM;

	ws2812->spi = spi;
	spin_lock_init(&ws2812->spi_lock);
	mutex_init(&ws2812->buf_lock);
	INIT_LIST_HEAD(&ws2812->device_entry);

	/* Register device node */
	mutex_lock(&device_list_lock);
	minor = find_first_zero_bit(minors, N_SPI_MINORS);
	if (minor < N_SPI_MINORS) {
		struct device *dev;

		ws2812->devt = MKDEV(WS2812_MAJOR, minor);
		dev = device_create(ws2812_class, &spi->dev, ws2812->devt,
				    ws2812, "WS2812");
		pr_info("Device created");
		status = PTR_ERR_OR_ZERO(dev);
	} else {
		pr_err("No minor number available!\n");
		status = -ENODEV;
	}
	if (status == 0) {
		set_bit(minor, minors);
		list_add(&ws2812->device_entry, &device_list);
	}
	mutex_unlock(&device_list_lock);
	ws2812->speed_hz = spi->max_speed_hz;
	config_gpio();
	if (status == 0)
		spi_set_drvdata(spi, ws2812);
	else
		kfree(ws2812);

	return status;
}

static int ws2812_remove(struct spi_device *spi)
{
	int retval = 0;
	struct ws2812_data *ws2812 = spi_get_drvdata(spi);
	pr_info("Remove invoked");
	ws2812->spi = NULL;
	device_destroy(ws2812_class, ws2812->devt);
	clear_bit(MINOR(ws2812->devt), minors);
	free_exportd_gpios();
	if (ws2812->users == 0)
		kfree(ws2812);
	return retval;
}
static struct spi_driver ws2812_spi_driver = {
	.driver =
		{
			.name = "ws2812",
			.owner = THIS_MODULE,
		},
	.probe = ws2812_probe,
	.remove = ws2812_remove,

};

static int __init ws2812_init(void)
{
	int status;
	pr_info("In Init function");
	status = register_chrdev(WS2812_MAJOR, "WS2812", &ws2812_fops);
	if (status < 0) {
		pr_err("register_chardev failed, error = %d", status);
		return status;
	}
	ws2812_class = class_create(THIS_MODULE, "WS2812");
	if (IS_ERR(ws2812_class)) {
		status = PTR_ERR(ws2812_class);
		goto error_class;
	}
	status = spi_register_driver(&ws2812_spi_driver);
	if (status < 0)
		goto error_register;


	if (busnum != -1 && chipselect != -1) {
		struct spi_board_info chip = {
			.modalias = "ws2812",
			.mode = spimode,
			.bus_num = busnum,
			.chip_select = chipselect,
			.max_speed_hz = maxspeed,
		};

		struct spi_master *master;

		master = spi_busnum_to_master(busnum);
		if (!master) {
			status = -ENODEV;
			goto error_busnum;
		}

		/* We create a virtual device that will sit on the bus
		 */
		spi = spi_new_device(master, &chip);
		if (!spi) {
			status = -EBUSY;
			goto error_mem;
		}
		pr_info("Device registered, busnum=%d cs=%d bufsiz=%d maxspeed=%d \n",
			busnum, chipselect, bufsiz, maxspeed);
	}
	return 0;
error_mem:
	pr_err("error_mem");
error_busnum:
	pr_err("error_busnum");
	spi_unregister_driver(&ws2812_spi_driver);
error_register:
	pr_err("error_register");
	class_destroy(ws2812_class);
error_class:
	pr_err("error_class");
	unregister_chrdev(WS2812_MAJOR, ws2812_spi_driver.driver.name);
	return status;
}
module_init(ws2812_init);

static void __exit ws2812_exit(void)
{
	pr_info("Exiting");
	if (spi) {
		spi_unregister_device(spi);
		spi = NULL;
	}
	spi_unregister_driver(&ws2812_spi_driver);
	class_destroy(ws2812_class);
	unregister_chrdev(WS2812_MAJOR, ws2812_spi_driver.driver.name);
}
module_exit(ws2812_exit);

MODULE_AUTHOR("Sree Gowtham Josyula <sreegowthamj@gmial.com>");
MODULE_DESCRIPTION("Platform driver for WS2812");
MODULE_LICENSE("GPL");
