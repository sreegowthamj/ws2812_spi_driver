/*****************************************************************************/
/*                ws2812_device.c : Platform device for WS2812               */
/*****************************************************************************/

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/module.h>
#include <linux/printk.h>
#include <linux/platform_device.h>

#include "ws2812_device.h"

static void ws2812_device_release(struct device *dev)
{
	pr_info("ws2812_device_release");
}

static ws2812_device_t ws2812_device0 = {
	.name = DEVICE_NAME,
	.dev_no = 1,
	.pf_dev = {.name = DEVICE_NAME,
		   .id = -1,
		   .dev = {
			   .release = ws2812_device_release,
		   }}};

static int __init ws2812_device_init(void)
{
	int ret = 0;
	platform_device_register(&ws2812_device0.pf_dev);
	pr_info("Register device ws2812 called\n");

	return ret;
}

static void __exit ws2812_device_exit(void)
{
	platform_device_unregister(&ws2812_device0.pf_dev);
	pr_info("Unregister device ws2812 called\n");
}

module_init(ws2812_device_init);
module_exit(ws2812_device_exit);

MODULE_AUTHOR("Sree Gowtham Josyula <sreegowthamj@gmial.com>");
MODULE_DESCRIPTION("Platform device for WS2812");
MODULE_LICENSE("GPL");
