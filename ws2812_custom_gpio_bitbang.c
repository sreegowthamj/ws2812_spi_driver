/**
 *   \file ws2812_custom_gpio_bitbang.c
 *   \brief Bitbanging GPIO for low latency during write.
 *
 *  Bitbanging GPIO for low latency during write
 *
 */

#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/miscdevice.h>
#include <linux/fs.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/platform_device.h>
#include <linux/gpio.h>
#include <linux/irq.h>
#include <linux/interrupt.h>
#include <linux/delay.h>
#include <linux/spinlock.h>
#include <asm/uaccess.h>
#include <linux/string.h>
#include <linux/slab.h>

#include "ws2812_consts.h"

static spinlock_t lock;
static long gpio_no = 12; // 28, 45
void __iomem *reg_base;
struct mutex buf_lock;
// static unsigned irq_no;
const static unsigned bufsiz = 16 * 3 * 8;

#define OUT_PIN 0
#define IN_PIN 1
#define HIGH 1
#define LOW 0

//u8 *tx_buffer;

typedef struct led_tx_data {
	uint8_t data_bit_expanded[3][8];
} led_tx_data;

static int export_gpio_pin(int gpio_pin, int io_pin_dir)
{
	int ret_val = 0;
	pr_info("GPIO pin in question = %d \n", gpio_pin);
	ret_val = gpio_request(gpio_pin, "sysfs");
	if (ret_val != 0) {
		pr_warn("LINUX_PIN allocation failed for GPIO pin = %d \n",
			gpio_pin);
		return -1;
	}
	if (io_pin_dir == OUT_PIN) {
		ret_val = gpio_direction_output(gpio_pin, 0);
		pr_info("setting gpio %d to output", gpio_pin);
		gpio_set_value_cansleep(gpio_pin, 0);
	} else if (io_pin_dir == IN_PIN) {
		pr_info("setting gpio %d to input", gpio_pin);
		ret_val = gpio_direction_input(gpio_pin);
	}
	if (ret_val != 0) {
		pr_warn("LINUX_PIN direction set failed for GPIO pin = %d \n",
			gpio_pin);
		return -1;
	}
	return 0;
}

static int config_level_shifter(int gpio_pin, int io_pin_dir)
{
	int ret_val = 0;
	pr_info("Level Shifter Configuration for gpio %d", gpio_pin);
	if (gpio_pin != -1) {
		ret_val = gpio_request(gpio_pin, "sysfs");
		if (ret_val != 0) {
			pr_warn("LEVEL_SHIFTER_PIN allocation failed for GPIO pin = %d",
				gpio_pin);
			return -1;
		}
		ret_val = gpio_direction_output(gpio_pin, io_pin_dir);
		pr_info("Setting Level Shifter gpio %d to output", gpio_pin);
		if (ret_val != 0) {
			pr_warn("LEVEL_SHIFTER_PIN direction set failed for GPIO pin = %d",
				gpio_pin);
			return -1;
		}
		/* TODO: See if the below causes errors */
		pr_info("Setting level shifter gpio %d to value %d", gpio_pin,
			io_pin_dir);
		gpio_set_value_cansleep(gpio_pin, io_pin_dir);
	}
	return 0;
}

/*****************************************************************************/
/*             custom method to write to gpio wooith low latency             */
/*****************************************************************************/

void get_reg_base(void)
{
	int gpio_no = 12, irq_num = 0;
	struct gpio_desc *desc;
	struct irq_chip_generic *chip_data;
	//	pr_info("5");
	desc = gpio_to_desc(gpio_no); //
	// pr_info("6");
	irq_num = gpiod_to_irq(desc);
	// pr_info("7");
	chip_data = (struct irq_chip_generic *)irq_get_chip_data(irq_num);
	// pr_info("8");
	reg_base = chip_data->reg_base;
	pr_info("REG_BASE = %d \n", *(int *)reg_base);
}

static int config_pin_mux1(int gpio_pin, int value)
{
	int ret_val = 0;
	if (gpio_pin != -1) {
		ret_val = gpio_request(gpio_pin, "sysfs");

		if (ret_val != 0) {
			pr_warn("PIN_MUX1 allocation failed for GPIO pin = %d",
				gpio_pin);
			return -1;
		}
		gpio_set_value_cansleep(gpio_pin, value);
		pr_info("Config Pin Mux 1 GPIO = %d, value = %d", gpio_pin,
			value);
	}

	return 0;
}


static int ws2812_custom_gpio_open(struct inode *inode, struct file *file)
{
	pr_info("Open \n");
	return 0;
}

static int ws2812_custom_gpio_close(struct inode *inodep, struct file *filp)
{
	pr_info("Close\n");
	return 0;
}

void byte_encode_tx_data(led_prop *src, led_tx_data *dst)
{
	int i;
	uint8_t j, k, count = 8;
	for (i = 2; i >= 0; i--) {
		j = src->color[i];
		//		pr_info("Source number = %d", j);
		count = 8;
		while (count != 0) {
			//			k = ((j << 1) >> 1);
			//			k = k - j;
			//			k = k >> 7;
			k = j - ((j >> 1) << 1);
			if (k == 1) {
				//				pr_info("Color =
				//%d, value = HIGH, bit = %d", i,
				// count);
				dst->data_bit_expanded[i][--count] = HIGH;
			} else if (k == 0) {
				//				pr_info("Color =
				//%d, value = LOW, bit = %d", i,
				// count);
				dst->data_bit_expanded[i][--count] = LOW;
			} else {
				--count;
				pr_err("Error in bit calculation");
			}
			j = j >> 1;
		}
	}
}

/**
 *  \brief qrk_gpio_set
 *
 *  Implements core bitbanging logic
 *
 *  \param value: bit value ( 1 / 0 )
 *  \return void
 */

static void qrk_gpio_set(uint8_t value)
{
	void __iomem *reg_data = reg_base;
	u32 val_data = 0;
	unsigned long flags = 0;
	unsigned offset = 4; // offset is 4 for gpio12

	spin_lock_irqsave(&lock, flags);

	val_data = ioread32(reg_data);
	if (value)
		iowrite32(val_data | BIT(offset % 32), reg_data);
	else
		iowrite32(val_data & ~BIT(offset % 32), reg_data);

	spin_unlock_irqrestore(&lock, flags);
}

static ssize_t ws2812_custom_gpio_write(struct file *filp,
					const char __user *buf, size_t count,
					loff_t *ppos)
{
	led_tx_data *tx_data;
	ssize_t status = 0;
	int i = 0, j = 0, k = 0;
	unsigned long missing;
	led_strip z;
	int data_size = 0;
	//	pr_info("Write invoked");

	pr_info("Write %d\n", count);
	/* chipselect only toggles at start or end of operation */
	if (count > bufsiz)
		return -EMSGSIZE;

	missing = copy_from_user(&z, buf, count);
	pr_info("Chk point 1");

	data_size = sizeof(led_prop) * (z.n);
	//	pr_info("Size of data received = %d, data being written = %d ",
	// count, 		data_size * 8);
	tx_data = kmalloc(data_size * 8,
			  GFP_KERNEL); // 8 is for the number of bits in a byte
	/* tx_buffer = kmalloc(data_size * 8, */
	/* 		  GFP_KERNEL); // 8 is for the number of bits in a byte */
	for (i = 0; i < z.n; i++) {
		byte_encode_tx_data(&z.led[i], &tx_data[i]);
	}

	pr_info("Chk point 3");
        	mutex_lock(&buf_lock);
        //	memcpy(tx_buffer, tx_data, 8 * data_size);
	pr_info("Chk point 4");
        //        while(1) {
	for (i = 0; i < z.n; i++) {
		for (j = 0; j < 3; j++) {
			for (k = 0; k < 8; k++) {
                          //                          pr_info("i = %d, j = %d, k = %d", i, j ,k);
				if (tx_data[i].data_bit_expanded[j][k]
				    == HIGH) {
					qrk_gpio_set(1);
					ndelay(550);
					qrk_gpio_set(0);
					ndelay(450);
				} else if (tx_data[i].data_bit_expanded[j][k]
					   == LOW) {
					qrk_gpio_set(1);
					ndelay(150);
					qrk_gpio_set(0);
					ndelay(900);
				}
			}
		}
	}
        //}
	usleep_range(55, 60);
	pr_info("Chk point 4");

        	mutex_unlock(&buf_lock);

	kfree(tx_data);
        //	kfree(tx_buffer);

	return status;
}

static const struct file_operations ws2812_custom_gpio_fops = {
	.owner = THIS_MODULE,
	.write = ws2812_custom_gpio_write,
	.open = ws2812_custom_gpio_open,
	.release = ws2812_custom_gpio_close,
};

struct miscdevice ws2812_custom_gpio_device = {
	.minor = MISC_DYNAMIC_MINOR,
	.name = "ws2812_custom_gpio_misc",
	.fops = &ws2812_custom_gpio_fops,
};

static int __init misc_init(void)
{
	int error;

	spin_lock_init(&lock);
	export_gpio_pin(12, OUT_PIN);
	config_level_shifter(28, OUT_PIN);
	config_pin_mux1(45, LOW);
	gpio_set_value_cansleep(12, 0);
	get_reg_base();
	mutex_init(&buf_lock);


	error = misc_register(&ws2812_custom_gpio_device);
	if (error) {
		pr_err("can't misc_register :(\n");
		return error;
	}

	pr_info("INIT\n");
	return 0;
}

static void __exit misc_exit(void)
{
	misc_deregister(&ws2812_custom_gpio_device);
	gpio_free(12);
	gpio_free(28);
	gpio_free(45);
	gpio_unexport(gpio_no);
	gpio_free(gpio_no);

	pr_info("EXIT\n");
}

module_init(misc_init);
module_exit(misc_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Sree Gowtham Josyula");
MODULE_DESCRIPTION("Control WS2812 led array using custom GPIO bitbanging");
MODULE_VERSION("0.1");
