#ifndef _WS2812_DEVICE_H
#define _WS2812_DEVICE_H

#define DEVICE_NAME "WS2812_PF_DEVICE"

typedef struct ws2812_device {
	char *name;
	int dev_no;
	struct platform_device pf_dev;
} ws2812_device_t;


#endif /* _WS2812_DEVICE_H */

