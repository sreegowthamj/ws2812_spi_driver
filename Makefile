MODULE = ws2812
obj-m += $(MODULE)_driver.o
obj-m += $(MODULE)_gpio_ndelay.o $(MODULE)_gpio_hrtimer.o
obj-m += gpio_5us_hrtimer.o gpio_5us_ndelay.o $(MODULE)_gpio_custom.o
obj-m += $(MODULE)_custom_gpio_bitbang.o

APP = $(MODULE)_test
APP2 = bitbang_$(MODULE)_test

# ###################################################################
check_defined = \
    $(strip $(foreach 1,$1, \
         $(call __check_defined,$1,$(strip $(value 2)))))
__check_defined = \
     $(if $(value $1),, \
       $(error Undefined $1$(if $2, ($2))))

$(call check_defined, CROSS_COMPILE, SDKTARGETSYSROOT, ARCH)

# ###################################################################

all: compile

compile:
	@make ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILE) -C $(SDKTARGETSYSROOT)/usr/src/kernel M=$(PWD) modules
	@$(CROSS_COMPILE)gcc -o $(APP) -Wall --sysroot=$(SDKTARGETSYSROOT) -lm -lpthread $(MODULE)_test.c
	@$(CROSS_COMPILE)gcc -o $(APP2) -Wall --sysroot=$(SDKTARGETSYSROOT) -lm -lpthread bitbang_$(MODULE)_test.c
clean:
	@make -C $(SDKTARGETSYSROOT)/usr/src/kernel M=$(PWD) clean 1> /dev/null
	rm -rf $(APP) $(APP2)

flash: compile
	scp $(MODULE)_driver.ko $(APP) $(APP2) $(MODULE)_custom_gpio_bitbang.ko $(MODULE)_gpio_custom.ko $(MODULE)_gpio_hrtimer.ko $(MODULE)_gpio_ndelay.ko gpio_5us_hrtimer.ko gpio_5us_ndelay.ko  root@192.168.1.5:/home/root/ 

# KDIR := /lib/modules/$(shell uname -r)/build
# PWD := $(shell pwd)

# CC := $(CROSS_COMPILE)gcc

# all:
# 	$(MAKE) -C $(KDIR) M=${shell pwd} modules

# clean:
# 	-$(MAKE) -C $(KDIR) M=${shell pwd} clean || true
# 	-rm *.o *.ko *.mod.{c,o} modules.order Module.symvers || true
