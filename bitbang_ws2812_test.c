#include <stdio.h>
#include <fcntl.h>  // for open
#include <unistd.h> // for close
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "ws2812_consts.h"

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

static const char *dev_node_ws = "/dev/ws2812_custom_gpio_misc";
int fd;
led_strip z;

void set_color(uint8_t *color)
{
	int i = 0;
	for (i = 0; i < z.n; i++) {
		memcpy(&z.led[i], color,
		       3 * sizeof(uint8_t)); /* TODO: 3 hardcoded here.. */
	}

	//	printf("sizeof(led_strip) = %d \n", sizeof(z));

	for (i = 0; i < 500; i = i) {
		write(fd, &z, sizeof(z));
		break;
	}
}

int main(int argc, char *argv[])
{
	int ret = 0;
	int i;
	void *x;
	uint8_t white[3] = {255, 255, 255};
	uint8_t off[3] = {0, 0, 0};
	/* uint8_t red[3] = {255, 0, 0}; */
	/* uint8_t blue[3] = {0, 255, 0}; */
	/* uint8_t green[3] = {0, 0, 255}; */
	uint8_t *y;
	z.n = 5;

	fd = open(dev_node_ws, O_RDWR);
	if (fd < 0) {
		perror("can't open device");
	}

	printf("%d \n", white[0]);

	x = (void *)calloc(1, sizeof(uint8_t) * 3);
	y = (void *)calloc(1, sizeof(uint8_t));

	memcpy(x, white, sizeof(uint8_t) * 3);
	memcpy(y, &white[2], sizeof(uint8_t));
	printf("%u \n", *y);
	/*********************************************************************/
	/*                Circular repeating white LED Pattern               */
	/*********************************************************************/
	while (1) {
		z.n = 16;
		set_color(off);
		printf("OFF \n");
		sleep(2);
		printf("White \n");
		usleep(10);
		for (i = 1; i <= 16; i++) {
			z.n = i;
			set_color(white);
			sleep(1);
		}
                break;
	}

	free(x);
	free(y);
	close(fd);

	return ret;
}
