/*****************************************************************************/
/*       ws2812_consts.h : Defines constants for ioctls and device name       */
/*****************************************************************************/

#ifndef WS2812_CONSTS_H
#define WS2812_CONSTS_H

#define GREEN 0
#define RED 1
#define BLUE 2


typedef struct led_properties {
	uint8_t color[3];
} led_prop;

typedef struct led_strip {
	led_prop led[16]; // max array of LED's
	int n;		  // number of LED's
} led_strip;


#define WS2812_IOC_MAGIC 'k'

#define WS2812_RESET _IO(WS2812_IOC_MAGIC, 1)


#endif /* WS2812_CONSTS_H */
