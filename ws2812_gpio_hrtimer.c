/**
 *   \file ws2812_gpio_hrtimer.c
 *   \brief Control WS2812 led array using GPIO and hrtimer
 *
 *  Control WS2812 led array using GPIO and hrtimer
 *
 */
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/platform_device.h>
#include <linux/gpio.h>
#include <linux/irq.h>
#include <linux/interrupt.h>
#include <linux/delay.h>

#define OUT_PIN 0
#define IN_PIN 1
#define HIGH 1
#define LOW 0

int pin_value = 0;
static struct hrtimer hr_timer;
unsigned long timer_interval_ns = 5000;

static int export_gpio_pin(int gpio_pin, int io_pin_dir)
{
	int ret_val = 0;
	pr_info("GPIO pin in question = %d \n", gpio_pin);
	ret_val = gpio_request(gpio_pin, "sysfs");
	if (ret_val != 0) {
		pr_warn("LINUX_PIN allocation failed for GPIO pin = %d \n",
			gpio_pin);
		return -1;
	}
	if (io_pin_dir == OUT_PIN) {
		ret_val = gpio_direction_output(gpio_pin, 0);
		pr_info("setting gpio %d to output", gpio_pin);
		gpio_set_value_cansleep(gpio_pin, 0);
	} else if (io_pin_dir == IN_PIN) {
		pr_info("setting gpio %d to input", gpio_pin);
		ret_val = gpio_direction_input(gpio_pin);
	}
	if (ret_val != 0) {
		pr_warn("LINUX_PIN direction set failed for GPIO pin = %d \n",
			gpio_pin);
		return -1;
	}
	return 0;
}

static int config_level_shifter(int gpio_pin, int io_pin_dir)
{
	int ret_val = 0;
	pr_info("Level Shifter Configuration for gpio %d", gpio_pin);
	if (gpio_pin != -1) {
		ret_val = gpio_request(gpio_pin, "sysfs");
		if (ret_val != 0) {
			pr_warn("LEVEL_SHIFTER_PIN allocation failed for GPIO pin = %d",
				gpio_pin);
			return -1;
		}
		ret_val = gpio_direction_output(gpio_pin, io_pin_dir);
		pr_info("Setting Level Shifter gpio %d to output", gpio_pin);
		if (ret_val != 0) {
			pr_warn("LEVEL_SHIFTER_PIN direction set failed for GPIO pin = %d",
				gpio_pin);
			return -1;
		}
		/* TODO: See if the below causes errors */
		pr_info("Setting level shifter gpio %d to value %d", gpio_pin,
			io_pin_dir);
		gpio_set_value_cansleep(gpio_pin, io_pin_dir);
	}
	return 0;
}

static int config_pin_mux1(int gpio_pin, int value)
{
	int ret_val = 0;
	if (gpio_pin != -1) {
		ret_val = gpio_request(gpio_pin, "sysfs");

		if (ret_val != 0) {
			pr_warn("PIN_MUX1 allocation failed for GPIO pin = %d",
				gpio_pin);
			return -1;
		}
		gpio_set_value_cansleep(gpio_pin, value);
		pr_info("Config Pin Mux 1 GPIO = %d, value = %d", gpio_pin,
			value);
	}

	return 0;
}

enum hrtimer_restart timer_callback(struct hrtimer *timer_for_restart)
{
	ktime_t currtime, interval;
	currtime = ktime_get();
	interval = ktime_set(0, timer_interval_ns);
	pin_value = !(pin_value);
	gpio_set_value_cansleep(12, pin_value);
	hrtimer_forward(timer_for_restart, currtime, interval);

	// set_pin_value(PIO_G,9,(cnt++ & 1)); //Toggle LED
	return HRTIMER_RESTART;
}

static int __init _gpio_access_init(void)
{
	/* long int i = 1000000; */
	ktime_t ktime;
	pr_info("WS2812 GPIO driver init");
	/* For IO1: GPIO PIN - 12, Level Shifter = gpio28(L), pin mux =
	 * gpio45(L) */
	/* gpio init *********************************************************/
	export_gpio_pin(12, OUT_PIN);
	config_level_shifter(28, OUT_PIN);
	config_pin_mux1(45, LOW);

	/* hrtimer init *****************************************************/
	ktime = ktime_set(0, timer_interval_ns);
	hrtimer_init(&hr_timer, CLOCK_MONOTONIC, HRTIMER_MODE_REL);
	hr_timer.function = &timer_callback;
	hrtimer_start(&hr_timer, ktime, HRTIMER_MODE_REL);

	return 0;
}

static void __exit _gpio_access_exit(void)
{
	int ret;
	pr_info("GPIO access exit");
	/* gpio_unexport(gpio_no); */
	/* gpio_free(gpio_no); */
	ret = hrtimer_cancel(&hr_timer);
	/* send value to the gpio ********************************************/
	gpio_free(12);
	gpio_free(28);
	gpio_free(45);

	if (ret)
		printk("The timer was still in use...\n");
	printk("hrtimer module exiting\n");
}

module_init(_gpio_access_init);
module_exit(_gpio_access_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Sree Gowtham Josyula");
MODULE_DESCRIPTION("Control WS2812 led array using GPIO and hrtimer");
MODULE_VERSION("0.1");
