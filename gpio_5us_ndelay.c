/**
 *   \file ws2812_gpio_ndelay.c
 *   \brief Control WS2812 led array using GPIO and ndelay
 *
 *  Control WS2812 led array using GPIO and ndelay
 *
 */
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/platform_device.h>
#include <linux/gpio.h>
#include <linux/irq.h>
#include <linux/interrupt.h>
#include <linux/delay.h>

static long gpio_no = 12; // 28, 45

#define OUT_PIN 0
#define IN_PIN 1
#define HIGH 1
#define LOW 0

static int export_gpio_pin(int gpio_pin, int io_pin_dir)
{
	int ret_val = 0;
	pr_info("GPIO pin in question = %d \n", gpio_pin);
	ret_val = gpio_request(gpio_pin, "sysfs");
	if (ret_val != 0) {
		pr_warn("LINUX_PIN allocation failed for GPIO pin = %d \n",
			gpio_pin);
		return -1;
	}
	if (io_pin_dir == OUT_PIN) {
		ret_val = gpio_direction_output(gpio_pin, 0);
		pr_info("setting gpio %d to output", gpio_pin);
		gpio_set_value_cansleep(gpio_pin, 0);
	} else if (io_pin_dir == IN_PIN) {
		pr_info("setting gpio %d to input", gpio_pin);
		ret_val = gpio_direction_input(gpio_pin);
	}
	if (ret_val != 0) {
		pr_warn("LINUX_PIN direction set failed for GPIO pin = %d \n",
			gpio_pin);
		return -1;
	}
	return 0;
}

static int config_level_shifter(int gpio_pin, int io_pin_dir)
{
	int ret_val = 0;
	pr_info("Level Shifter Configuration for gpio %d", gpio_pin);
	if (gpio_pin != -1) {
		ret_val = gpio_request(gpio_pin, "sysfs");
		if (ret_val != 0) {
			pr_warn("LEVEL_SHIFTER_PIN allocation failed for GPIO pin = %d",
				gpio_pin);
			return -1;
		}
		ret_val = gpio_direction_output(gpio_pin, io_pin_dir);
		pr_info("Setting Level Shifter gpio %d to output", gpio_pin);
		if (ret_val != 0) {
			pr_warn("LEVEL_SHIFTER_PIN direction set failed for GPIO pin = %d",
				gpio_pin);
			return -1;
		}
		/* TODO: See if the below causes errors */
		pr_info("Setting level shifter gpio %d to value %d", gpio_pin,
			io_pin_dir);
		gpio_set_value_cansleep(gpio_pin, io_pin_dir);
	}
	return 0;
}

static int config_pin_mux1(int gpio_pin, int value)
{
	int ret_val = 0;
	if (gpio_pin != -1) {
		ret_val = gpio_request(gpio_pin, "sysfs");

		if (ret_val != 0) {
			pr_warn("PIN_MUX1 allocation failed for GPIO pin = %d",
				gpio_pin);
			return -1;
		}
		gpio_set_value_cansleep(gpio_pin, value);
		pr_info("Config Pin Mux 1 GPIO = %d, value = %d", gpio_pin,
			value);
	}

	return 0;
}

static int __init _gpio_access_init(void)
{
  long int i = 0;
	pr_info("WS2812 GPIO driver init");
	/* For IO1: GPIO PIN - 12, Level Shifter = gpio28(L), pin mux =
	 * gpio45(L) */
	export_gpio_pin(12, OUT_PIN);
	config_level_shifter(28, OUT_PIN);
	config_pin_mux1(45, LOW);
	while (i != 1000000 ) {
		gpio_set_value_cansleep(12, 1);
		ndelay(5000);
		gpio_set_value_cansleep(12, 0);
		i++;
	}

	gpio_free(12);
	gpio_free(28);
	gpio_free(45);

	return 0;
}

static void __exit _gpio_access_exit(void)
{
	pr_info("GPIO access exit");
	gpio_unexport(gpio_no);
	gpio_free(gpio_no);
}

module_init(_gpio_access_init);
module_exit(_gpio_access_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Sree Gowtham Josyula");
MODULE_DESCRIPTION("Control WS2812 led array using GPIO and ndelay");
MODULE_VERSION("0.1");
