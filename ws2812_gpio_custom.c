/**
 *   \file ws2812_gpio_ndelay.c
 *   \brief Control WS2812 led array using GPIO with a custom method
 *
 *  Control WS2812 led array using GPIO with a custom method to reduce delays
 *
 */
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/platform_device.h>
#include <linux/gpio.h>
#include <linux/irq.h>
#include <linux/interrupt.h>
#include <linux/delay.h>
#include <linux/spinlock.h>

static spinlock_t lock;
static long gpio_no = 12; // 28, 45
void __iomem *reg_base;
// static unsigned irq_no;

#define OUT_PIN 0
#define IN_PIN 1
#define HIGH 1
#define LOW 0


static int export_gpio_pin(int gpio_pin, int io_pin_dir)
{
	int ret_val = 0;
	pr_info("GPIO pin in question = %d \n", gpio_pin);
	ret_val = gpio_request(gpio_pin, "sysfs");
	if (ret_val != 0) {
		pr_warn("LINUX_PIN allocation failed for GPIO pin = %d \n",
			gpio_pin);
		return -1;
	}
	if (io_pin_dir == OUT_PIN) {
		ret_val = gpio_direction_output(gpio_pin, 0);
		pr_info("setting gpio %d to output", gpio_pin);
		gpio_set_value_cansleep(gpio_pin, 0);
	} else if (io_pin_dir == IN_PIN) {
		pr_info("setting gpio %d to input", gpio_pin);
		ret_val = gpio_direction_input(gpio_pin);
	}
	if (ret_val != 0) {
		pr_warn("LINUX_PIN direction set failed for GPIO pin = %d \n",
			gpio_pin);
		return -1;
	}
	return 0;
}

static int config_level_shifter(int gpio_pin, int io_pin_dir)
{
	int ret_val = 0;
	pr_info("Level Shifter Configuration for gpio %d", gpio_pin);
	if (gpio_pin != -1) {
		ret_val = gpio_request(gpio_pin, "sysfs");
		if (ret_val != 0) {
			pr_warn("LEVEL_SHIFTER_PIN allocation failed for GPIO pin = %d",
				gpio_pin);
			return -1;
		}
		ret_val = gpio_direction_output(gpio_pin, io_pin_dir);
		pr_info("Setting Level Shifter gpio %d to output", gpio_pin);
		if (ret_val != 0) {
			pr_warn("LEVEL_SHIFTER_PIN direction set failed for GPIO pin = %d",
				gpio_pin);
			return -1;
		}
		/* TODO: See if the below causes errors */
		pr_info("Setting level shifter gpio %d to value %d", gpio_pin,
			io_pin_dir);
		gpio_set_value_cansleep(gpio_pin, io_pin_dir);
	}
	return 0;
}

/*****************************************************************************/
/*             custom method to write to gpio wooith low latency             */
/*****************************************************************************/

static void qrk_gpio_set(int value)
{
	void __iomem *reg_data = reg_base;
	u32 val_data = 0;
	unsigned long flags = 0;
	unsigned offset = 4; // offset is 4 for gpio12

	spin_lock_irqsave(&lock, flags);

	val_data = ioread32(reg_data);
	if (value)
		iowrite32(val_data | BIT(offset % 32), reg_data);
	else
		iowrite32(val_data & ~BIT(offset % 32), reg_data);

	spin_unlock_irqrestore(&lock, flags);
}

void write_gpio_data(int data)
{
	int gpio_no = 12, irq_num = 0;
	struct gpio_desc *desc;
	struct irq_chip_generic *chip_data;
	//	pr_info("5");
	desc = gpio_to_desc(gpio_no); //
	// pr_info("6");
	irq_num = gpiod_to_irq(desc);
	// pr_info("7");
	chip_data = (struct irq_chip_generic *)irq_get_chip_data(irq_num);
	// pr_info("8");
	reg_base = chip_data->reg_base;
	pr_info("REG_BASE = %d \n", *(int *)reg_base);
}

static int config_pin_mux1(int gpio_pin, int value)
{
	int ret_val = 0;
	if (gpio_pin != -1) {
		ret_val = gpio_request(gpio_pin, "sysfs");

		if (ret_val != 0) {
			pr_warn("PIN_MUX1 allocation failed for GPIO pin = %d",
				gpio_pin);
			return -1;
		}
		gpio_set_value_cansleep(gpio_pin, value);
		pr_info("Config Pin Mux 1 GPIO = %d, value = %d", gpio_pin,
			value);
	}

	return 0;
}

static int __init _gpio_access_init(void)
{
	long int i = 0, count = 0;
	pr_info("WS2812 GPIO driver init");
	spin_lock_init(&lock);
	/* For IO1: GPIO PIN - 12, Level Shifter = gpio28(L), pin mux =
	 * gpio45(L) */
	export_gpio_pin(12, OUT_PIN);
	config_level_shifter(28, OUT_PIN);
	config_pin_mux1(45, LOW);
	gpio_set_value_cansleep(12, 0);
	write_gpio_data(i);

	/*********************************************************************/
	/*                  Experiment 1 : Circular Green pattern            */
	/*********************************************************************/
	while (1) {
		/* gpio_set_value_cansleep(12, 1); */
		qrk_gpio_set(1);
		ndelay(5000);
		/* gpio_set_value_cansleep(12, 0); */
		qrk_gpio_set(0);
		ndelay(450);
	}

	while (i != 10000) { // 1000000
		i++;
		if (i % 8 == 0) {
			usleep_range(55, 60);
			count++;
		}
		if (i % 24 <= 7) {
			/* gpio_set_value_cansleep(12, 1); */
			qrk_gpio_set(1);
			ndelay(550);
			/* gpio_set_value_cansleep(12, 0); */
			qrk_gpio_set(0);
			ndelay(450);
		} else {
			/* gpio_set_value_cansleep(12, 1); */
			qrk_gpio_set(1);
			ndelay(150);
			/* gpio_set_value_cansleep(12, 0); */
			qrk_gpio_set(0);
			ndelay(900);
		}
	}
	i = 0;
	/* while (i != 100000) { */
	/* 	i++; */
	/* 	if (i % 384 == 0) { */
	/* 		usleep_range(55, 60); */
	/* 		count++; */
	/* 	} */
	/* 	gpio_set_value_cansleep(12, 1); */
	/* 	ndelay(100); */
	/* 	gpio_set_value_cansleep(12, 0); */
	/* 	ndelay(1000); */
	/* } */

	gpio_free(12);
	gpio_free(28);
	gpio_free(45);

	return 0;
}

static void __exit _gpio_access_exit(void)
{
	pr_info("GPIO access exit");
	gpio_unexport(gpio_no);
	gpio_free(gpio_no);
}

module_init(_gpio_access_init);
module_exit(_gpio_access_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Sree Gowtham Josyula");
MODULE_DESCRIPTION("Control WS2812 led array using GPIO and ndelay");
MODULE_VERSION("0.1");
